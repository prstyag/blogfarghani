<?php 

use App\Http\Controllers\Admin\AuthController;
// use App\Http\Controllers\Admin\CommentController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->name('admin.')->group(function() {
    Route::view('/login', 'auth.login'); 
    Route::post('/login', [AuthController::class, 'store']); // admin login

    Route::group(['namespace' => 'App\Http\Controllers\Admin', 'middleware' => 'auth'], function(){
        Route::get('/', 'AdminController@index')->name('welcome');
        Route::get('dashboard', 'AdminController@index')->name('dashboard');
        Route::get('/profile', 'AdminController@showProfile')->name('user.profile');
        Route::get('/profile/{id}/edit', 'AdminController@editProfile')->name('user.edit.profile');
        Route::put('/profile/{id}/update', 'AdminController@updateProfile')->name('user.update.profile');


        //  Roles Control Owner, Admin, Blogwritter
        Route::get('/post/trash', [App\Http\Controllers\Admin\PostController::class, 'trashed'])->name('post.trash');
        Route::get('/post/restore/{id}', [App\Http\Controllers\Admin\PostController::class, 'restore'])->name('post.restore');
        Route::delete('/post/delete/{id}', [App\Http\Controllers\Admin\PostController::class, 'kill'])->name('post.forcedelete');
        Route::get('/post/draft', [App\Http\Controllers\Admin\PostController::class, 'draftPost'])->name('post.draft');
        // Route::delete('/comment/{id}', 'CommentController@destroy')->name('comment.destroy');
        Route::resource('comment-reply', CommentReplyController::class);
        Route::resource('comment', CommentController::class);
        Route::resource('post', PostController::class);

        //  Roles Control Owner, Admin
        Route::resource('category', CategoryController::class);
        Route::resource('tag', TagController::class);

        //  Roles Control Owner
        Route::resource('users', UsersController::class);
        Route::resource('permission', PermissionController::class);
        Route::resource('roles', RolesController::class);
    });
});