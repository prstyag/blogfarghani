<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\CommentReplyController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::post('comment/{post}', [CommentController::class, 'store'])->name('comment');
Route::delete('comment/{post}', [CommentController::class, 'destroy'])->name('comment_destroy');
Route::post('comment-reply/{comment}', [CommentReplyController::class, 'store'])->name('comment_replay');
Route::delete('comment-reply/{comment}', [CommentReplyController::class, 'destroy'])->name('comment_replay_destroy');

// Users & All Roles Control Panel Routes
Route::group(['prefix' => 'user', 'as' => 'user.', 'namespace' => 'App\Http\Controllers\User', 'middleware' => ['auth']], function() {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('posts', 'DashboardController@posts')->name('posts');
    Route::get('posts/read/{slug}', 'DashboardController@read')->name('read');
});
// Owner, Admin & Blogwriter Control Panel Routes
// Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'App\Http\Controllers\Admin', 'middleware' => 'auth'], function(){
//     Route::get('/', 'AdminController@index')->name('welcome');
//     Route::get('dashboard', 'AdminController@index')->name('dashboard');

        //  Roles Control Owner, Admin, Blogwriter
    // Route::resource('post', PostController::class);

        //  Roles Control Owner, Admin
    // Route::resource('category', CategoryController::class);
    // Route::resource('tag', TagController::class);
    
        //  Roles Control Owner,
//     Route::resource('users', UsersController::class);
//     Route::resource('permission', PermissionController::class);
//     Route::resource('roles', RolesController::class);
// });

require __DIR__.'/admin.php';