@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger" style="text-align: center; font-weight:bold;">
            {{$error}}
        </div>
    @endforeach
@endif

@if (session('success'))
    <div class="alert alert-success" style="text-align: center; font-weight:bold;">
        {{session('success')}}
    </div>
@endif

@if (session('info'))
    <div class="alert alert-warning" style="text-align: center; font-weight:bold;">
        {{session('info')}}
    </div>
@endif

@if (session('error'))
    <div class="alert alert-danger" style="text-align: center; font-weight:bold;">
        {{session('error')}}
    </div>
@endif