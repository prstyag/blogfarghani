
<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="favicon.ico">

        <!--Google Font link-->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">


        <link rel="stylesheet" href="{{asset('user/css/slick/slick.css')}}"> 
        <link rel="stylesheet" href="{{asset('user/css/slick/slick-theme.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/iconfont.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/magnific-popup.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/bootsnav.css')}}">

        <!--Theme custom css -->
        <link rel="stylesheet" href="{{asset('user/css/style.css')}}">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="{{asset('user/css/responsive.css')}}" />
        @stack('links')

        <script src="{{asset('user/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
    </head>

    <body data-spy="scroll" data-target=".navbar-collapse">


        <!-- Preloader -->
        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object" id="object_one"></div>
                    <div class="object" id="object_two"></div>
                    <div class="object" id="object_three"></div>
                    <div class="object" id="object_four"></div>
                </div>
            </div>
        </div><!--End off Preloader -->


        <div class="culmn">
            <!--Home page style-->

                <!--Navbar page style-->
                @include('layouts.components.navbar')
                <!--Navbar page style-->

                <!--Navbar page style-->
                {{-- @include('layouts.components.home') --}}
                <div class="awokwokw">
                        @yield('content')
                </div>
                <!--Navbar page style-->

                <!--Footer has Section-->
                @include('layouts.components.footer')
                <!--Footer has Section-->
        </div>

        <!-- JS includes -->

        <script src="{{asset('user/js/vendor/jquery-1.11.2.min.js') }}"></script>
        <script src="{{asset('user/js/vendor/bootstrap.min.js') }}"></script>

        <script src="{{asset('user/js/owl.carousel.min.js') }}"></script>
        <script src="{{asset('user/js/jquery.magnific-popup.js') }}"></script>
        <script src="{{asset('user/js/jquery.easing.1.3.js') }}"></script>
        <script src="{{asset('user/css/slick/slick.js') }}"></script>
        <script src="{{asset('user/css/slick/slick.min.js') }}"></script>
        <script src="{{asset('user/js/jquery.collapse.js') }}"></script>
        <script src="{{asset('user/js/bootsnav.js') }}"></script>



        <script src="{{asset('user/js/plugins.js') }}"></script>
        <script src="{{asset('user/js/main.js') }}"></script>
        @stack('script')
    </body>
</html>




{{-- <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif
                            
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>
                                
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @auth
                                        @if(Auth::check())
                                            @if (Auth::user()->isAdmin())
                                                <a href="{{ url('/admin/dashboard') }}" class="dropdown-item text-sm text-gray-700 underline">Admin</a>
                                            @else
                                                <a href="{{ url('/user/dashboard') }}" class="dropdown-item text-sm text-gray-700 underline">User</a>
                                            @endif
                                        @endif
                                    @endauth
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html> --}}
