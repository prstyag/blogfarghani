@section('sub-title', 'Dashboard')
@section('location', 'Dashboard')
<div class="main-content">
    <section class="section">
    <div class="section-header">
        <div class="col-sm-6">
            <h1>@yield('location')</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">@yield('location')</li>
            </ol>
        </div>
    </div>
    @include('include.message')
    @yield('content')
    </section>
</div>
