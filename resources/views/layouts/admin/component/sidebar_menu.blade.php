<div class="main-sidebar">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="{{route('home')}}">{{ config('app.name') }}</a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{route('home')}}">FN</a>
      </div>
      <ul class="sidebar-menu">
        @role(['owner','admin', 'blogwriter'])
          <li class="menu-header">Dashboard Menu</li>
          <li class="nav-item dropdown">
          <a href="{{route('admin.dashboard')}}" class="nav-link {{ Request::is('dashboard') ? ' active' : null }}"><i class="fas fa-fire"></i><span>Dashboard</span></a>
          </li>
          {{-- <li class="menu-header">Starter</li> --}}
          <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-newspaper"></i> <span>Post</span></a>
            <ul class="dropdown-menu">
              <li><a class="nav-link beep beep-sidebar" href="{{route('admin.post.create')}}"><i class="far fa-plus-square" style="margin: 0"></i>Add Post</a></li>
              <li><a class="nav-link" href="{{route('admin.post.index')}}"><i class="fas fa-list-ul" style="margin: 0"></i>List Post</a></li>
              <li><a class="nav-link" href="{{route('admin.post.draft')}}"><i class="fas fa-list-ul" style="margin: 0"></i>Draft Post</a></li>
              <li><a class="nav-link" href="{{route('admin.comment.index')}}"><i class="far fa-comment" style="margin: 0"></i>Post Comment</a></li>
              <li><a class="nav-link" href="{{route('admin.comment-reply.index')}}"><i class="far fa-comments" style="margin: 0"></i>Post Comment reply</a></li>
            </ul>
          </li>
          
          <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-stream"></i> <span>Kategory</span></a>
            <ul class="dropdown-menu">
              <li><a class="nav-link beep beep-sidebar" href="{{route('admin.category.create')}}"><i class="far fa-plus-square" style="margin: 0"></i>Add Kategory</a></li>
              <li><a class="nav-link" href="{{route('admin.category.index')}}"><i class="fas fa-list-ul" style="margin: 0"></i>List Kategory</a></li>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fab fa-slack-hash"></i> <span>Tag</span></a>
            <ul class="dropdown-menu">
              <li><a class="nav-link beep beep-sidebar" href="{{route('admin.tag.create')}}"><i class="far fa-plus-square" style="margin: 0"></i>Add Tags</a></li>
              <li><a class="nav-link" href="{{route('admin.tag.index')}}"><i class="fas fa-list-ul" style="margin: 0"></i>List Tags</a></li>
            </ul>
          </li>
          @role(['owner','admin'])
            <li class="menu-header">SDM & Permissions</li>
            <li><a class="nav-link" href="{{route('admin.users.index')}}"><i class="fas fa-user"></i> <span>Users</span></a></li>
          @endrole
          @role(['owner'])
            <li><a class="nav-link" href="{{route('admin.roles.index')}}"><i class="fas fa-eye-slash"></i> <span>Roles</span></a></li>
            <li><a class="nav-link" href="{{route('admin.permission.index')}}"><i class="fas fa-info"></i> <span>Permissions</span></a></li>
            <li><a class="nav-link" href="{{route('admin.post.trash')}}"><i class="far fa-trash-alt"></i> <span>Trash Bin</span></a></li>
          @endrole
          @endrole
        </ul>

        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
          <a href="{{ url('/') }}" class="btn btn-primary btn-lg btn-block btn-icon-split">
            <i class="fas fa-rocket"></i> {{ config('app.name') }}
          </a>
        </div> 
    </aside>
  </div>