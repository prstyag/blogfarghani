@extends('layouts.app')
@section('content')
@push('links')
<link rel="stylesheet" href="{{asset('user/css/custom.css')}}">

<style>
    .card-blog {
        margin-top: 30px;
    }

    .card-search {
        margin-bottom: 30px;
    }

    .card-post {
        margin-top: 30px;
    }
</style>
<script src="https://cdn.jsdelivr.net/sharer.js/latest/sharer.min.js"></script>
@endpush


  <!-- Page Content -->
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
  <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">{{$posts->title}}</h1>
        <!-- Author -->
        <p style="margin-top: 0;">Posted on {{ $posts->created_at->format('F d, Y') }} by
          <a href="#">{{$posts->user->username }}</a>
        </p>
        <div class="list-category">
            <div class="advance-btn">{{ $posts->category->name }}</div>
        </div>
        <hr>

        <!-- Preview Image -->
        <img class="img-fluid rounded" src="{{asset('storage/post/' .$posts->gambar)}}" alt="{{$posts->slug}}" width="900" height="300">
        <!-- Post Content -->
        
        <p class="lead">{{$posts->subtitle}}</p>

        <p>{!! $posts->content !!}</p><br>

        <div class="row">
          <hr>
            <div class="col-md-6">
                <ul class="tags"> 
                    <h5 style="font-size: 1.5rem;">Tags:</h5>
                    @foreach ($posts->tags as $tag)
                    <li><a href="#" style="font-size: 1rem;text-transform: capitalize;font-weight: 700;color: cornflowerblue;">{{ $tag->name }} <span>{{$tag->posts->count()}}</span></a></li>
                    @endforeach
                </ul> 
            </div>
            <div class="col-md-6">
                <h2 class="social-title">Share This Post:</h2>
                <div class="social">
                  <a href="#" id="share-wa" class="sharer button"><i class="fa fa-3x fa-whatsapp"></i></a>
                  <a href="#" id="share-fb" class="sharer button"><i class="fa fa-3x fa-facebook-square"></i></a>
                  <a href="#" id="share-tw" class="sharer button"><i class="fa fa-3x fa-twitter-square"></i></a>
                  <a href="#" id="share-li" class="sharer button"><i class="fa fa-3x fa-linkedin-square"></i></a>
                  <a href="#" id="share-gp" class="sharer button"><i class="fa fa-3x fa-google-plus-square"></i></a>
                </div>
  
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col-lg-12" style="margin-bottom: 3em;">
              <div class="comments-area">
                {{-- <h4>{{$posts->comments->count()}} Comments</h4> --}}
                <h4>
                  @if ($posts->comments->count() > 0)
                      {{$posts->comments->count()}} comments

                      {{-- @foreach ($n_comment as $item)
                      {{$item->comments_count}} komentar
                      @endforeach

                      @foreach ($n_reply as $item)
                      {{$item->replies_count}} reply
                      @endforeach --}}
                      
                  @else
                      Tidak ada komentar
                  @endif
                </h4>
                @foreach ($posts->comments as $comment)
                <!-- Frist Comment -->
                <div class="comment">
                  <div class="comment-list">
                    <div class="single-comment justify-content-between d-flex">
                      <div class="user justify-content-between d-flex">
                        <div class="thumb">
                          <img src="{{ asset('storage/photo/'.$comment->user->photo) }}" alt="{{$comment->user->username}}" width="60px">
                        </div>
                        <div class="desc">
                          <h5><a href="#">{{$comment->user->username}}</a>
                              {{-- checking for comment/reply writter --}}
                            @if ($posts->user_id == $comment->user->id)
                              <i class="fa fa-check-circle" style="color: deepskyblue"></i>
                              {{-- <i class="fa fa-pen-square" style="color: deepskyblue"></i> --}}
                            @else
                            @endif
                          </h5>
                          <p class="date">{{$comment->created_at->format('D, d M Y H:i')}}</p>
                          <p class="comment">
                            {{$comment->comment}}
                          </p>
                        </div>
                      </div>
                      <div class="btn-comment">
                        <button class="btn-reply text-uppercase" id="reply-btn" 
                      onclick="showReplyForm('{{$comment->id}}','{{$comment->user->username}}')">reply</button
                        >
                        @if ($comment->user_id == Auth::id())
                          <form action="{{route('comment_destroy', $comment->id)}}" method="POST">
                            @csrf
                            @method('delete')
                            <button class="btn-reply2 text-uppercase btn-action" style="">delete</button>
                          </form>
                        @else
                        @endif
                      </div>
                    </div>
                  </div>
                  @if ($comment->replies->count() > 0)
                      @foreach ($comment->replies as $reply)
                      <div class="comment-list left-padding">
                        <div class="single-comment justify-content-between d-flex">
                          <div class="user justify-content-between d-flex">
                            <div class="thumb">
                              <img src="{{ asset('storage/photo/'.$reply->user->photo) }}" alt="{{$reply->user->username}}" width="60px">
                            </div>
                            <div class="desc">
                              <h5><a href="#">{{$reply->user->username}}</a> 
                                {{-- checking for comment/reply writter --}}
                              @if ($posts->user_id == $reply->user->id)
                                  <i class="fa fa-check-circle" style="color: deepskyblue"></i>
                              @else
                              @endif
                              </h5>
                              <p class="date">{{$reply->created_at->format('D, d M Y H:i')}}</p>
                              <p class="comment">
                                {{$reply->message}}
                              </p>
                            </div>
                          </div>
                          <div class="btn-comment">
                            <button class="btn-reply text-uppercase" id="reply-btn" 
                              onclick="showReplyForm('{{$comment->id}}','{{$reply->user->username}}')">reply</button>
                            @if ($reply->user_id == Auth::id())
                            <form action="{{route('comment_replay_destroy', $reply->id)}}" method="POST">
                              @csrf
                              @method('delete') 
                              <button class="btn-action text-uppercase btn-reply2" type="submit">delete</button>
                            </form>
                            @else
                            @endif
                          </div>
                        </div>
                      </div>
                      @endforeach
                  @else
                      
                  @endif
                  <div class="comment-list left-padding" id="reply-form-{{$comment->id}}" style="display: none">
                    <div
                      class="single-comment justify-content-between d-flex"
                    >
                      <div class="user justify-content-between d-flex">
                        <div class="thumb">
                          <img src="img/asset/c2.jpg" alt="" />
                        </div>
                        <div class="desc">
                          <h5><a href="#">{{Auth::user()->username}}</a></h5>
                          <p class="date">{{date('D, d M Y H:i')}}</p>
                          <div class="row flex-row d-flex">
                          <form action="{{route('comment_replay', $comment->id)}}" method="POST">
                            @csrf
                            <div class="col-lg-12">
                              <textarea
                                id="reply-form-{{$comment->id}}-text"
                                cols="60"
                                rows="2"
                                class="form-control mb-10"
                                name="message"
                                placeholder="Messege"
                                onfocus="this.placeholder = ''"
                                onblur="this.placeholder = 'Messege'"
                                required=""
                              ></textarea>
                            </div>
                            <button type="submit" class="btn-reply text-uppercase ml-3">Send</button>
                          </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                {{-- <div class="comment-list">
                  <div class="single-comment justify-content-between d-flex">
                    <div class="user justify-content-between d-flex">
                      <div class="thumb">
                        {{-- <i class="fa fa-user" style="font-size:30px; width:30px;"></i>
                      <img src="{{ asset('storage/photo/'.$comment->user->photo) }}" alt="{{$comment->user->name}}" width="60px">
                      </div>
                      <div class="desc">
                        <h5>{{$comment->user->name}}</h5>
                        <p class="date">{{$comment->created_at->format('D, d M Y H:i')}}</p>
                        <p class="comment" style="color: #364547;">
                          {{$comment->comment}}
                        </p>
                      </div>
                    </div>
                    <div class="reply-btn">
                    <button class="btn-reply text-uppercase" id="reply-btn" onclick="showReplyForm('{{$comment->id}}', '{{$comment->user->name}}')">reply</button>
                    </div>
                  </div>
                </div> --}}
                @endforeach
                {{-- <div class="comment-list left-padding">
                  <div class="single-comment justify-content-between d-flex">
                    <div class="user justify-content-between d-flex">
                      <div class="thumb">
                        <i class="fa fa-user" style="font-size:30px; width:30px;"></i>
                      </div>
                      <div class="desc">
                        <h5>Elsie Cunningham</h5>
                        <p class="date">December 4, 2017 at 3:12 pm </p>
                        <p class="comment">
                          I am Reply of above comment.
                        </p>
                      </div>
                    </div>
                    <div class="reply-btn">
                      <a href="" class="btn-reply text-uppercase">reply</a>
                    </div>
                  </div>
                </div>
                <div class="comment-list left-padding" id="reply-form-{{$comment->id}}" style="display: none;">
                  <div class="single-comment justify-content-between d-flex">
                    <div class="user justify-content-between d-flex">
                      <div class="thumb">
                        <i class="fa fa-user" style="font-size:30px; width:30px;"></i>
                      </div>
                      <div class="desc">
                        <h5>Elsie Cunningham</h5>
                        <p class="date">December 4, 2017 at 3:12 pm </p>
                        <p class="comment">
                          I am Reply of above comment.
                        </p>
                      </div>
                    </div>
                    <div class="reply-btn">
                      <a href="" class="btn-reply text-uppercase">reply</a>
                    </div>
                  </div>
                </div> --}}
              </div>
            </div>
        </div>

        <!-- Comments Form -->
        <div class="card my-4" style="margin-bottom: 5em;">
          <h5 class="card-header">Leave a Comment:</h5>
          <div class="card-body">
          <form action="{{route('comment', $posts->id)}}" method="POST">
            @csrf
              <div class="form-group">
                <textarea class="form-control" rows="3" name="comment" placeholder="This Comments .."></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>

        <!-- Single Comment -->
        {{-- <div class="media mb-4">
          <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
          <div class="media-body">
            <h5 class="mt-0">Commenter Name</h5>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
          </div>
        </div> --}}

        <!-- Comment with nested comments -->
        {{-- <div class="media left-padding mb-4">
          <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
          <div class="media-body">
            <h5 class="mt-0">Commenter Name</h5>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.

            <div class="media mt-4">
              <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
              <div class="media-body">
                <h5 class="mt-0">Commenter Name</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
              </div>
            </div>

            <div class="media mt-4">
              <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
              <div class="media-body">
                <h5 class="mt-0">Commenter Name</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
              </div>
            </div>

          </div>
        </div> --}}

      </div>

      <!-- Sidebar Widgets Column -->
      <div class="col-md-4">

        <!-- Search Widget -->
        <div class="card-search my-4">
          <div class="card-body">
            <div class="input-group">
                <form class="form-search" method="get" action="#">
                    <input type="search" name="search" placeholder="search posts here..">
                    <button type="submit">Search</button>
                </form>
            </div>
          </div>
        </div>

        <!-- Category -->
        <div class="single category">
            <h3 class="side-title">Category</h3>
            <ul class="list-unstyled">
              @forelse ($categories as $category)
                <li><a href="#">{{$category->name}}</a></li>
              @empty
              @endforelse
            </ul>
            <hr>
        </div>
        <!-- /Category -->

        <div class="card-post my-4">
            <div class="sidebar widget">
                <h3>Recent Post</h3>
                <ul>
                  @forelse ($latestPosts as $latest)
                  <li>
                      <div class="sidebar-thumb">
                      <img class="animated rollIn" src="{{asset('storage/post/' .$latest->gambar)}}" alt="{{$latest->title}}" />
                      </div><!-- .Sidebar-thumb -->
                      <div class="sidebar-content">
                          <h5 class="animated bounceInRight"><a href="{{route('user.read', $latest->slug)}}">{{$latest->title}}</a></h5>
                      </div><!-- .Sidebar-thumb -->
                          <div class="sidebar-meta">
                          <span class="time" ><i class="fa fa-clock-o"></i> {{ $latest->created_at->format('M d, Y') }}</span>
                          <span class="comment"><i class="fa fa-comment"></i> {{$latest->comments->count()}} comments</span>
                      </div><!-- .Sidebar-meta ends here -->
                  </li>
                  @empty
                      
                  @endforelse
            </div>
        </div>
      </div>

    </div>
  </div>
@endsection
@push('script')
<script type="text/javascript">
  // comment-reply
  function showReplyForm(commentId,username) {
    var x = document.getElementById(`reply-form-${commentId}`);
    var input = document.getElementById(`reply-form-${commentId}-text`);

    if (x.style.display === "none") {
      x.style.display = "block";
      input.innerText=`@${username} `;

    } else {
      x.style.display = "none";
    }
  };

</script>
@endpush
