@extends('layouts.admin.app')
@section('sub-title', 'Permissions')
@section('location', 'Permissions')
@section('content')

<div class="clearfix mt-4">
  <a href="{{route('admin.permission.create')}}" class="btn btn-info mb-2">Add Permission</a>
</div>
      
      {{-- <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Permission</h1>
      </div>
      <a class="btn btn-sm btn-primary" href="{{route('permission.create')}}">Add New Permission</a>
      <h2>{{$title}}</h2> --}}
      <div class="card">
        <div class="card-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Name</th>
                <th>Display Name</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach($permissions as $row)
                <tr>
                  <td>{{ $row->name }}</td>
                  <td>{{ $row->display_name }}</td>
                  <td>{{ $row->description }}</td>
                  <td>
                    <div>
                      <a class="btn btn-primary" href="{{ route('admin.permission.edit', $row->id) }}" class="btn btn-info " data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i> </a>
                      <a class="btn btn-danger" href="{{ route('admin.permission.show', $row->id) }}" class="btn btn-danger " data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i> </a>
                    </div>
                  </td>
                </tr>
                @endforeach
            </tbody>
          </table>
          <!-- Pager -->
          <div class="clearfix mt-4">
            {{$permissions->links('vendor.pagination.bootstrap-4')}}
          </div>
        </div>
      </div>

@endsection