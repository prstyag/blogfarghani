@extends('layouts.admin.app')
@section('sub-title', 'Permissions')
@section('location', 'Permissions / Edit')
@section('content')
<form method="POST" action="{{ route('admin.permission.update', $permission->id) }}" data-parsley-validate>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-sm btn-primary" href="{{route('admin.permission.index')}}">Back</a>
                    <h4 class="ml-3">{{$title}}</h4>
                </div>
                <div class="card-body">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
                        <label for="name" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" value="{{$permission->name}}" id="name" name="name" class="form-control col-md-7 col-xs-12"> @if ($errors->has('name'))
                            <span class="help-block">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }} row">
                        <label for="display_name" class="col-sm-2 col-form-label">Display Name</label>
                        <div class="col-sm-10">
                            <input type="text" value="{{$permission->display_name}}" id="display_name" name="display_name" class="form-control col-md-7 col-xs-12"> @if ($errors->has('display_name'))
                            <span class="help-block">{{ $errors->first('display_name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }} row">
                        <label for="description" class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <input type="text" value="{{$permission->description}}" id="description" name="description" class="form-control col-md-7 col-xs-12"> @if ($errors->has('description'))
                            <span class="help-block">{{ $errors->first('description') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-sm-2 col-form-label text-md-right">
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                            <input name="_method" type="hidden" value="PUT">
                            <button type="submit" class="btn btn-primary">Update Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection