@extends('layouts.admin.app')
@section('sub-title', 'Permissions')
@section('location', 'Permissions / Delete')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <a class="btn btn-sm btn-primary" href="{{route('admin.permission.index')}}">Back</a>
                <h4 class="ml-3">{{$title}}</h4>
            </div>
            <div class="card-body">
                <p>Are you sure you want to delete
                    <strong><u> {{$permission->name}}</u></strong>
                </p>

                <form method="POST" action="{{ route('admin.permission.destroy', $permission->id) }}">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    <input name="_method" type="hidden" value="DELETE">
                    <button type="submit" class="btn btn-danger">Yes I'm sure. Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection