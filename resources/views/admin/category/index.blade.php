@extends('layouts.admin.app')
@section('sub-title', 'Category')
@section('location', 'Category')
@section('content')

    <div class="clearfix mt-4">
    <a href="{{route('admin.category.create')}}" class="btn btn-info mb-2">Add Kategory</a>
    </div>
    
    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Name Category</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    @if (count($categories) > 0)
                    <tbody>
                        @foreach ($categories as $key => $category)
                        <tr>
                        <th scope="row">{{ $key+1 }}.</th>
                        <td>{{ \Carbon\Carbon::parse($category->created_at)->format('d/m/Y')}}</td>
                        <td>{{ $category->name }}</td>
                        <td>
                            <form action="{{ route('admin.category.destroy', $category->id) }}" method="POST">
                                @csrf
                                @method('delete')
                                <a class="btn btn-info" href="{{ route('admin.category.show', $category->id) }}" data-toggle="tooltip" title="View"><i class="fas fa-eye"></i> </a>
                                <a href="{{ route('admin.category.edit', $category->id) }}" class="btn btn-primary btn-action " data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                                <button type="submit" class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                        {{$categories->links('vendor.pagination.bootstrap-4')}}
                    @else
                        <td colspan="4" class="text-center" style="color: #6777ef;">                           
                            <b>Category not found. <a href="{{route('admin.category.create')}}" style="font-weight: bold;">Create here!</a></b>
                        </td>
                    @endif
            </table>
        </div>
    </div>
@endsection