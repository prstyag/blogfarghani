@extends('layouts.admin.app')
@section('sub-title', 'Users')
@section('location', 'Users / Create')
@push('links')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css"> 
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('admin.users.index')}}" class="btn btn-info">Back</a>
                <h4 class="ml-3">{{$title}}</h4>
            </div>
            <div class="card-body">
                <form action="{{route('admin.users.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
                        <label for="name" class="col-sm-2 col-form-label">Name
                            <span class="required">*</span>
                        </label>
                        <div class="col-sm-10">
                            <input type="text" value="{{ Request::old('name') ?: '' }}" id="name" name="name" placeholder="Enter name" class="form-control col-md-7 col-xs-12"> @if ($errors->has('name'))
                            <span class="help-block">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }} row">
                        <label for="username" class="col-sm-2 col-form-label">Username
                            <span class="required">*</span>
                        </label>
                        <div class="col-sm-10">
                            <input type="text" value="{{ Request::old('username') ?: '' }}" id="username" name="username" placeholder="Enter username" class="form-control col-md-7 col-xs-12"> @if ($errors->has('username'))
                            <span class="help-block">{{ $errors->first('username') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} row">
                        <label for="address" class="col-sm-2 col-form-label">Address
                            <span class="required">*</span>
                        </label>
                        <div class="col-sm-10">
                            <input type="text" value="{{ Request::old('address') ?: '' }}" id="address" name="address" placeholder="Enter address" class="form-control col-md-7 col-xs-12"> @if ($errors->has('address'))
                            <span class="help-block">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} row">
                        <label for="phone" class="col-sm-2 col-form-label">Phone
                            <span class="required">* <span style="font-size: 10px; color:#a6a9b6">min:11|max:13</span></span>
                        </label>
                        <div class="col-sm-10">
                            <input type="text" value="{{ Request::old('phone') ?: '' }}" id="phone" name="phone" placeholder="Enter mobile number with prefix 08 or 62" class="form-control col-md-7 col-xs-12"> @if ($errors->has('phone'))
                            <span class="help-block" style="color: red">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div>
            
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                        <label for="email" class="col-sm-2 col-form-label">Email
                            <span class="required">*</span>
                        </label>
                        <div class="col-sm-10">
                            <input type="text" value="{{ Request::old('email') ?: '' }}" id="email" name="email" placeholder="Enter email" class="form-control col-md-7 col-xs-12"> @if ($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
            
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row">
                        <label for="password" class="col-sm-2 col-form-label">Password
                            <span class="required">*</span>
                        </label>
                        <div class="col-sm-10">
                            <input type="password" value="{{ Request::old('password') ?: '' }}" id="password" name="password" placeholder="Enter password" class="form-control col-md-7 col-xs-12"> @if ($errors->has('password'))
                            <span class="help-block">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
            
                    <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }} row">
                        <label for="confirm_password" class="col-sm-2 col-form-label">Confirm Password
                            <span class="required">*</span>
                        </label>
                        <div class="col-sm-10">
                            <input type="password" value="{{ Request::old('confirm_password') ?: '' }}" id="confirm_password" placeholder="Enter confirm password" name="confirm_password"
                                class="form-control col-md-7 col-xs-12"> @if ($errors->has('confirm_password'))
                            <span class="help-block">{{ $errors->first('confirm_password') }}</span>
                            @endif
                        </div>
                    </div>
            
                    {{-- <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Role</label>
                        <div class="col-md-6 col-sm-6">
                          <div class="selectgroup w-100">
                            <label class="selectgroup-item">
                              <input type="radio" name="value" value="developer" class="selectgroup-input">
                              <span class="selectgroup-button">Owner</span>
                            </label>
                            <label class="selectgroup-item">
                              <input type="radio" name="value" value="ceo" class="selectgroup-input">
                              <span class="selectgroup-button">Admin</span>
                            </label>
                            <label class="selectgroup-item">
                                <input type="radio" name="value" value="ceo" class="selectgroup-input">
                                <span class="selectgroup-button">Writter</span>
                              </label>
                          </div>
                        </div>
                    </div> --}}

                    <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }} row">
                        <label class="col-sm-2 col-form-label" for="category_id">Role
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="selectpicker form-control" id="role_id" name="role_id">
                                <option value="" disabled selected>Select your role</option>
                                @if(count($roles)) @foreach($roles as $row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach @endif
                            </select>
                            @if ($errors->has('role_id'))
                            <span class="help-block">{{ $errors->first('role_id') }}</span>
                            @endif
                        </div>
                    </div>
            
                    <div class="ln_solid"></div>
            
                    <div class="form-group">
                        <div class="col-sm-2 col-form-label text-md-right">
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                            <button type="submit" class="btn btn-primary">Create User</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4"> --}}

    {{-- <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Users</h1>
    </div>
    <a class="btn btn-sm btn-primary" href="{{route('users.index')}}">Back</a>
    <h2>{{$title}}</h2>
    <form method="post" action="{{ route('users.store') }}" data-parsley-validate class="form-horizontal form-label-left">

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
            <label for="name" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input type="text" value="{{ Request::old('name') ?: '' }}" id="name" name="name" class="form-control col-md-7 col-xs-12"> @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="text" value="{{ Request::old('email') ?: '' }}" id="email" name="email" class="form-control col-md-7 col-xs-12"> @if ($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row">
            <label for="password" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input type="password" value="{{ Request::old('password') ?: '' }}" id="password" name="password" class="form-control col-md-7 col-xs-12"> @if ($errors->has('password'))
                <span class="help-block">{{ $errors->first('password') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }} row">
            <label for="confirm_password" class="col-sm-2 col-form-label">Confirm Password</label>
            <div class="col-sm-10">
                <input type="password" value="{{ Request::old('confirm_password') ?: '' }}" id="confirm_password" name="confirm_password"
                    class="form-control col-md-7 col-xs-12"> @if ($errors->has('confirm_password'))
                <span class="help-block">{{ $errors->first('confirm_password') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }} row">
            <label class="col-sm-2 col-form-label" for="category_id">Role
                <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control" id="role_id" name="role_id">
                    @if(count($roles)) @foreach($roles as $row)
                    <option value="{{$row->id}}">{{$row->name}}</option>
                    @endforeach @endif
                </select>
                @if ($errors->has('role_id'))
                <span class="help-block">{{ $errors->first('role_id') }}</span>
                @endif
            </div>
        </div>

        <div class="ln_solid"></div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <input type="hidden" name="_token" value="{{ Session::token() }}">
                <button type="submit" class="btn btn-success">Create User</button>
            </div>
        </div>
    </form>
</main> 
</div>
</div> --}}

@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
@endpush