@extends('layouts.admin.app')
@section('sub-title', 'Edit Profile')
@section('location', 'Edit Profile')
@section('content') 

<form method="POST" action="{{ route('admin.user.update.profile', $user->id) }}" enctype="multipart/form-data"  data-parsley-validate>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('admin.user.profile')}}" class="btn btn-info">Back</a>
                    <h4 class="ml-3">Edit Profile</h4>
                </div>
                <div class="card-body">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
                        <label for="name" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" value="{{$user->name}}" id="name" name="name" class="form-control col-md-7 col-xs-12"> @if ($errors->has('name'))
                            <span class="help-block">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }} row">
                        <label for="username" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                            <input type="text" value="{{ $user->username}}" id="username" name="username"  class="form-control col-md-7 col-xs-12"> @if ($errors->has('username'))
                            <span class="help-block">{{ $errors->first('username') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} row">
                        <label for="address" class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10">
                            <input type="text" value="{{ $user->address }}" id="address" name="address" class="form-control col-md-7 col-xs-12"> @if ($errors->has('address'))
                            <span class="help-block">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} row">
                        <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                        <div class="col-sm-10">
                            <input type="text" value="{{ $user->phone }}" id="phone" name="phone" class="form-control col-md-7 col-xs-12"> @if ($errors->has('phone'))
                            <span class="help-block" style="color: red">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div>
            

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                        <label for="email" class="col-sm-2 col-form-label ">Email</label>
                        <div class="col-sm-10">
                            <input type="text" value="{{$user->email}}" id="email" name="email" class="form-control col-md-7 col-xs-12"> @if ($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Avatar</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control col-md-7 col-xs-12" name="photo" onchange="previewFile(this)" />
                            <img id="previewImg" src="{{asset('storage/photo/')}}/{{$user->photo}}" alt="" style="max-width: 130px;margin-top: 20px;max-height: 130px;" />
                        </div>
                    </div>
                    {{-- <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }} row">
                        <label class="col-sm-2 col-form-label" for="category_id">Role
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="selectpicker form-control" id="role_id" name="role_id">
                                {{-- @foreach ($roles as $role)
                            <option value="{{ $role->id }}"
                                @foreach ($user->roles as $item)
                                    @if ($role->id == $item->id)
                                        selected
                                    @endif
                                @endforeach
                                >{{ $role->name }}</option>
                                @endforeach
                                @if(count($roles))
                                @foreach($roles as $row)
                                <option value="{{$row->id}}" {{$row->id == $user->roles[0]->id ? 'selected="selected"' : ''}}>{{$row->name}}</option>
                                @endforeach
                                @endif
                            </select>
                            @if ($errors->has('role_id'))
                            <span class="help-block">{{ $errors->first('role_id') }}</span>
                            @endif
                        </div>
                    </div> --}}

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-sm-2 col-form-label text-md-right">
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                            <input name="_method" type="hidden" value="PUT">
                            <button type="submit" class="btn btn-primary">Update Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection
