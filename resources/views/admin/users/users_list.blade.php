@extends('layouts.admin.app')
@push('links')
<style>
  .results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px; 
  color:#ccc;
}
}
</style>
@endpush
@section('sub-title', 'Users')
@section('location', 'Users')
@section('content')
  
      <div class="clearfix mt-4">
        <a href="{{route('admin.users.create')}}" class="btn btn-info mb-2">Add User</a>
      </div>
      {{-- <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Users</h1>
      </div>
      <a class="btn btn-sm btn-primary" href="{{route('users.create')}}">Add New User</a> --}}
      {{-- <h2>{{$title}}</h2> --}}
      <div class="card">
        <div class="card-body">
          <div class="form-group pull-right">
            <input type="text" class="search form-control" placeholder="What you looking for?">
          </div>
          <span class="counter pull-right"></span>
          <table class="table table-bordered table-striped table-hover results"
          id="table"
        >
            <thead>
              <tr>
                <th data-field="state" data-checkbox="true"><input id="selectAll" type="checkbox"><label for='selectAll'></label></th>
                <th data-field="prenom" data-filter-control="input" data-sortable="true" scope="col">No</th>
                <th data-field="prenom" data-filter-control="input" data-sortable="true" scope="col">Username</th>
                <th data-field="prenom" data-filter-control="input" data-sortable="true" scope="col">Email</th>
                <th data-field="prenom" data-filter-control="select" data-sortable="true" scope="col">Role</th>
                <th data-field="prenom" data-filter-control="select" data-sortable="true" scope="col">Created At</th>
                <th scope="col">Action</th>
              </tr>
              <tr class="warning no-result">
                <td colspan="7"><i class="fa fa-warning"></i> No result</td>
              </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                      <td class="bs-checkbox "><input data-index="0" name="btSelectItem" type="checkbox"></td>
                      <td>{{ ++$i }}</td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>
                      @foreach($user->roles as $r)
                          {{$r->display_name}}
                      @endforeach
                      </td>
                      <td>
                        {{ $user->created_at->format('d/m/Y') }}
                      </td>
                      <td>
                          {{-- <form action="{{ route('users.show', $user->id) }}" method="POST">
                              @csrf
                              @method('delete')
                              <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary btn-action " data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                              <button type="submit" class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i></button>
                            </form> --}}
                        <div>
                            <a class="btn btn-info" href="{{ route('admin.users.show', $user->id) }}" data-toggle="tooltip" title="View"><i class="fas fa-eye"></i> </a>
                            <a class="btn btn-primary" href="{{ route('admin.users.edit', $user->id) }}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i> </a>
                            <a class="btn btn-danger" href="{{ route('admin.users.show', $user->id) }}" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i> </a>
                        </div>
                      </td>
                    </tr>
                    @endforeach
            </tbody>
          </table>
          <!-- Pager -->
          <div class="clearfix mt-4">
            {{$users->links('vendor.pagination.bootstrap-4')}}
          </div>
        </div>
      </div>
@endsection
@push('scripts')
<script>

$(document).ready(function() {
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
    
  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });
    
  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });

  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' item');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();}
		});

    $("#selectAll").click(function(){
        $("input[type=checkbox]").prop('checked', $(this).prop('checked'));
    });
});

//   $(document).ready(function(){
//   $("#myInput").on("keyup", function() {
//     var value = $(this).val().toLowerCase();
//     $("#myTable tr").filter(function() {
//       $(this).toggle($(this).find('td:eq(0), td:eq(1)').text().replace(/\s+/g, ' ').toLowerCase().indexOf(value) > -1)
//     });
//   });
// });
</script>
@endpush