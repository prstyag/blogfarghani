@extends('layouts.admin.app')
@section('sub-title', 'Comment Replies')
@section('location', 'Replies')
@section('content')
<div class="card">
    <div class="animated fadeIn">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        {{-- <th scope="col">No.</th> --}}
                        <th scope="col">Created at</th>
                        <th scope="col">Replied Comment</th>
                        <th scope="col">Replied User</th>
                        <th scope="col">Comment</th>
                        <th scope="col">Comment User</th>
                        <th scope="col">Post</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    @if (count($replies) > 0)
                    <tbody>
                        @foreach ($replies as $key => $reply)
                        <tr>
                        {{-- <th scope="row">{{ $key+1 }}.</th> --}}
                        <td>{{ $reply->created_at->diffForHumans()}}</td>
                        <td>{{ Str::limit($reply->message, 12) }}</td>
                        <td>{{ $reply->user->name }}</td>
                        <td>{{ Str::limit($reply->comment->comment, 12) }}</td>
                        <td>{{ $reply->comment->user->name }}</td>
                        <td><a href="{{route('admin.post.show', $reply->comment->post->slug)}}">{{ $reply->comment->post->title }}</a></td>
                        <td>
                            <form action="{{ route('admin.comment-reply.destroy', $reply->id) }}" method="POST">
                                @csrf
                                @method('delete') 
                                {{-- <a class="btn btn-info" href="{{ route('admin.category.show', $category->id) }}" data-toggle="tooltip" title="View"><i class="fas fa-eye"></i> </a>
                                <a href="{{ route('admin.category.edit', $category->id) }}" class="btn btn-primary btn-action " data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a> --}}
                                <button type="submit" class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete" data-target="#deleteModal-{{$reply->id}}"><i class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                        {{$replies->links('vendor.pagination.bootstrap-4')}}
                    @else
                        <td colspan="4" class="text-center" style="color: #6777ef;">                           
                            <b>Replied not found. <a href="#" style="font-weight: bold;">Create here!</a></b>
                        </td>
                    @endif
                </div>
            </table>
        </div>
    </div>
</div>
@endsection
