@extends('layouts.admin.app')
@section('sub-title', 'Post')
@section('location', 'Post / Add Post')
@push('links')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css"> 

@endpush
@section('content') 
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('admin.post.index')}}" class="btn btn-info">Back</a>
                <h4 class="ml-3">Add Post</h4>
            </div>
            <div class="card-body">
                <form action="{{route('admin.post.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tittle</label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" required class="form-control" name="title" placeholder="Create your title..">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kategory</label>
                        <div class="col-sm-12 col-md-7">
                            <select class="selectpicker form-control" data-live-search="true" name="category_id" required>
                                <option value="" disabled selected>Select your category</option>
                                @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tag</label>
                        <div class="col-sm-12 col-md-7">
                            <select class="selectpicker form-control select2" multiple data-live-search="true" name="tags[]" id="" required>
                                @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Status</label>
                        <div class="col-sm-12 col-md-7">
                            <select class="selectpicker form-control" data-live-search="true" name="status" required>
                                <option value="" disabled selected>Select status</option>
                                <option value="draft">Draft</option>
                                <option value="publish">Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                        <div class="col-sm-12 col-md-7">
                            <textarea name="content" id="content" cols="73" rows="4" class="form-control" placeholder="Create your content.." required></textarea>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Thumbnail <i class="fas fa-info-circle ml-2" data-toggle="tooltip" data-placement="top" title="Max 1 Mb" style="cursor: pointer"></i> </label>
                        <div class="col-sm-12 col-md-7">
                            <input type="file" class="form-control" name="gambar" onchange="previewFile(this)" required/>
                            <img id="previewImg" src="" alt="" style="max-width: 130px;margin-top: 20px;max-height: 130px;" />
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                        <div class="col-sm-12 col-md-7">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
@endpush