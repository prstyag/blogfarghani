@extends('layouts.admin.app')
@section('sub-title', 'Post')
@section('location', 'Details Post')
@push('links')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>

            /* Tags */
          .tags {
                  list-style: none;
                  margin: 0;
                  overflow: hidden; 
                  padding: 0;
                }

          .tag {
            background: #eee;
            border-radius: 3px 0 0 3px;
            color: #999;
            display: inline-block;
            height: 26px;
            line-height: 26px;
            padding: 0 20px 0 23px;
            position: relative;
            margin: 0 10px 10px 0;
            text-decoration: none;
            -webkit-transition: color 0.2s;
          }

          .tag::before {
            background: #fff;
            border-radius: 10px;
            box-shadow: inset 0 1px rgba(0, 0, 0, 0.25);
            content: '';
            height: 6px;
            left: 10px;
            position: absolute;
            width: 6px;
            top: 10px;
          }

          .tag::after {
            background: #fff;
            border-bottom: 13px solid transparent;
            border-left: 10px solid #eee;
            border-top: 13px solid transparent;
            content: '';
            position: absolute;
            right: 0;
            top: 0;
          }

          .tag:hover {
            background-color: #6777ef;
            color: white;
          }

          .tag:hover::after {
            border-left-color: #6777ef; 
          }

          /* Share */
        .fa {
              padding: 15px;
              width: 50px;
              font-size: 20px;
              text-align: center;
              text-decoration: none;
              border-radius: 50%;
            }

          /* Add a hover effect if you want */
        .fa:hover {
          opacity: 0.7;
        }

        .fa-facebook {
          background: #3B5998;
          color: white;
        }

        .fa-twitter {
          background: #55ACEE;
          color: white;
        }

        .fa-google {
          background: #dd4b39;
          color: white;
        }

        .fa-linkedin {
          background: #007bb5;
          color: white;
        }
        .fa-pinterest {
          background: #cb2027;
          color: white;
        }

        .img-post{
          width: 100%;
          height: 300px;
        }
    </style>
@endpush
@section('content') 
    <div class="clearfix mt-4">
        <a href="{{route('admin.post.index')}}" class="btn btn-info mb-2">Back</a>
    </div>

    <div class="section-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="tickets">
                    <div class="ticket-content">
                        <div class="ticket-header">
                            <div class="ticket-detail">
                              <div class="ticket-title">
                                <h4>{{$posts->title}}</h4>
                              </div>
                              <div class="ticket-info" style="font-size: 11px">
                                  <span class="text-primary font-weight-600"> {{$posts->created_at->diffForHumans()}}</span><div style="display: inline-block;" class="bullet"></div><span>by <a href="#" style="text-decoration: none;">{{$posts->user->name }} </a>
                              </div><br>
                            </div>
                          </div>
                        <div class="ticket-description">
                            @if (file_exists('storage/post/'.$posts->gambar))
                              <div class="ticket-sender-picture img-shadow">
                                <img class="img-post" src="{{asset('storage/post/' .$posts->gambar)}}" alt="image {{$posts->title}}">
                              </div> <br>
                            @else
                            <h6 style="color: #cb2027"><i>Upps.. Thumbnail post is not found!</i></h6>
                              {{-- <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS52y5aInsxSm31CvHOFHWujqUx_wWTS9iM6s7BAm21oEN_RiGoog" class="img-thumbnail" alt="{{$posts->title}}'s Profile Pic"> --}}
                            @endif
                            <p>{{$posts->content}}</p>

                            <div class="ticket-divider"></div> 
                            
                            <hr>
                            <div class="row">
                                <div class="col-md-6 gallerry">
                                    <h5 class="tags">Tags:</h5><br>
                                    @foreach ($posts->tags as $tag)
                                        <a href="#" class="tag" style="text-decoration: none">
                                          {{ $tag->name }}&ensp;
                                        </a>
                                    @endforeach
                                </div>
                                <div class="col-md-6 gallerry">
                                    <h5>Share:</h5>
                                        <!-- facebook -->
                                        <a class="facebook" href="https://www.facebook.com/share.php?u={{route('admin.post.show', $posts->slug)}}&title={{$posts->title}}" target="blank"><i class="fa fa-facebook"></i></a>
                                      
                                        <!-- twitter -->
                                        <a class="twitter" href="https://twitter.com/intent/tweet?status={{$posts->title}}+{{route('admin.post.show', $posts->slug)}}" target="blank"><i class="fa fa-twitter"></i></a>
                                      
                                        <!-- google plus -->
                                        <a class="googleplus" href="https://plus.google.com/share?url={{route('admin.post.show', $posts->slug)}}" target="blank"><i class="fa fa-google"></i></a>
                                      
                                        <!-- linkedin -->
                                        <a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url={{route('admin.post.show', $posts->slug)}}&title={{$posts->title}}&source=#" target="blank"><i class="fa fa-linkedin"></i></a>
                                        
                                        <!-- pinterest -->
                                        <a class="pinterest" href="https://pinterest.com/pin/create/bookmarklet/?media=#&url={{route('admin.post.show', $posts->slug)}}&is_video=false&description={{$posts->title}}" target="blank"><i class="fa fa-pinterest"></i></a>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
@endsection
