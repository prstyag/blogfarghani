@extends('layouts.admin.app')
@section('sub-title', 'Trash Bin')
@section('location', 'Trash Bin')
@section('content') 

    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Date</th>
                        <th scope="col">Name Post</th>
                        <th scope="col">Kategory</th>
                        <th scope="col">Cover</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    @if (count($posts) > 0)
                    @foreach ($posts as $key => $post)
                    <tbody>
                    <tr>
                        <th scope="row">{{ $key+1 }}</th>
                        <td>{{ $post->created_at->format('d/m/Y') }}</td>
                        <td><a href="">{{ $post->title }}</a></td>
                        <td>{{ $post->category->name }}</td>
                    <td><img src="{{ asset('images/'.$post->gambar) }}" alt="{{ $post->judul }}" style="max-width: 50px;margin-top: 2px;"></td>
                        <td>
                        <form action="{{route('admin.post.forcedelete', $post->id) }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="{{route('admin.post.restore', $post->id) }}" class="btn btn-primary btn-action " data-toggle="tooltip" title="Restored"><i class="fas fa-reply"></i></a>
                            <button type="submit" class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i></button>
                        </form>
                        </td>
                    </tr>
                    </tbody>
                    @endforeach
                    @else
                        <td colspan="6" class="text-center" style="color: #6777ef;">                           
                            <b>No items here.</b>
                        </td>
                    @endif
            </table>
        </div>
    </div>
    <!-- Pager -->
    <div class="clearfix mt-4">
        {{$posts->links('vendor.pagination.bootstrap-4')}}
    </div>
@endsection