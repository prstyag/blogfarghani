@extends('layouts.admin.app')
@section('sub-title', 'Tag')
@section('location', 'Tag  /   Edit Tag')
@section('content')

<form action="{{route('admin.tag.update', $tags->id )}}" method="POST">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('admin.tag.index')}}" class="btn btn-info">Back</a>
                    <h4 class="ml-3">Edit Tag</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tag</label>
                        <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="name" value="{{ $tags->name }}">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                        <div class="col-sm-12 col-md-7">
                            <button class="btn btn-primary">Update Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection