@extends('layouts.admin.app')
@section('sub-title', 'Tag')
@section('location', 'Tag')
@section('content')

    <div class="clearfix mt-4">
    <a href="{{route('admin.tag.create')}}" class="btn btn-info mb-2">Add Tag</a>
    </div>
    
    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Name Category</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    @if (count($tags) > 0)
                    @foreach ($tags as $key => $tag)
                    <tbody>
                        <tr>
                        <th scope="row">{{ $key+1 }}.</th>
                        <td>{{ $tag->created_at->format('d/m/Y') }}</td>
                        <td>{{ $tag->name }}</td>
                        <td>
                        <form action="{{ route('admin.tag.destroy', $tag->id) }}" method="POST">
                            @csrf
                            @method('delete')
                            <a class="btn btn-info" href="{{ route('admin.tag.show', $tag->id) }}" data-toggle="tooltip" title="View"><i class="fas fa-eye"></i> </a>
                            <a href="{{ route('admin.tag.edit', $tag->id) }}" class="btn btn-primary btn-action " data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                            <button type="submit" class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i></button>
                        </form>
                        </td>
                    </tr>
                    </tbody>
                    @endforeach
                        {{$tags->links('vendor.pagination.bootstrap-4')}}
                    @else
                        <td colspan="4" class="text-center" style="color: #6777ef;">
                            <b>Tag not found. <a href="{{route('admin.tag.create')}}" style="font-weight: bold;">Create here!</a></b>
                        </td>
                    @endif
            </table>
        </div>
    </div>
@endsection