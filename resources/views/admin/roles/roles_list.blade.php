@extends('layouts.admin.app')
@section('sub-title', 'Role')
@section('location', 'Role')
@section('content')

    <a class="btn btn-sm btn-info mb-2" href="{{route('admin.roles.create')}}">Add New Role</a>
      <div class="card">
        <div class="card-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">Role Display</th>
                <th scope="col">Role Description</th>
                <th scope="col">Role</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach($roles as $role)
                <tr>
                  <td>{{ $role->display_name }}</td>
                  <td>{{ $role->description }}</td>
                  <td>{{ $role->name }}</td>
                  <td>
                    <div>
                      <a class="btn btn-primary" href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-info" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                      <a class="btn btn-danger" href="{{ route('admin.roles.show', $role->id) }}" class="btn btn-danger" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i></a>
                    </div>
                  </td>
                </tr>
                @endforeach
            </tbody>
          </table>
          <div class="clearfix mt-4">
            {{$roles->links('vendor.pagination.bootstrap-4')}}
          </div>
        </div>
      </div>
@endsection