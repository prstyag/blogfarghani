@extends('layouts.admin.app')
@section('sub-title', 'Posts Comment')
@section('location', 'Comment')
@section('content')

    <div class="card">
        <div class="animated fadeIn">
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Created at</th>
                            <th scope="col">Comment</th>
                            <th scope="col">User</th>
                            <th scope="col">Post</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        @if (count($comments) > 0)
                        <tbody>
                            @foreach ($comments as $key => $comment)
                            <tr>
                            <th scope="row">{{ $key+1 }}.</th>
                            <td>{{ $comment->created_at->diffForHumans()}}</td>
                            <td>{{ $comment->comment }}</td>
                            <td>{{ $comment->user->name }}</td>
                            <td><a href="{{route('admin.post.show', $comment->post->slug)}}">{{ $comment->post->title }}<a/></td>
                            <td>
                                <form action="{{ route('admin.comment.destroy', $comment->id) }}" method="POST">
                                    @csrf
                                    @method('delete') 
                                    {{-- <a class="btn btn-info" href="{{ route('admin.category.show', $category->id) }}" data-toggle="tooltip" title="View"><i class="fas fa-eye"></i> </a>
                                    <a href="{{ route('admin.category.edit', $category->id) }}" class="btn btn-primary btn-action " data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a> --}}
                                    <button type="submit" class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete" data-target="#deleteModal-{{$comment->id}}"><i class="fas fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                            {{$comments->links('vendor.pagination.bootstrap-4')}}
                        @else
                            <td colspan="4" class="text-center" style="color: #6777ef;">                           
                                <b>Comment not found. <a href="#" style="font-weight: bold;">Create here!</a></b>
                            </td>
                        @endif
                    </div>
                </table>
            </div>
        </div>
        <!-- .animated -->
        {{-- <div class="animated">
            @foreach ($comments as $comment)
            <div class="modal fade" id="deleteModal-{{$comment->id}}" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel"
                data-backdrop="static" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticModalLabel">Delete Comment</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                The Comment will be deleted !!
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-danger" onclick="event.preventDefault();
                            document.getElementById('deletecomment-{{$comment->id}}').submit();">Confirm</button>
                    <form action="{{route('admin.comment.destroy', $comment->id)}}" style="display: none" id="deletecomment-{{$comment->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                    </form>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div> --}}
    </div>
@endsection
@push('scripts')
    {{-- Show-Hidden --}}
    <script type="text/javascript">
        function showReplyForm(commentId,user) {
          var x = document.getElementById(`reply-form-${commentId}`);
          var input = document.getElementById(`reply-form-${commentId}-text`);
          if (x.style.display === "none") {
            x.style.display = "block";
            input.innerText=`@${user} `;
          } else {
            x.style.display = "none";
          }
        };
    </script>
@endpush