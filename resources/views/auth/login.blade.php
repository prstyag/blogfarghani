<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('login-form/fonts/icomoon/style.css')}}">
    <link rel="stylesheet" href="{{asset('login-form/css/owl.carousel.min.css')}}">

    <link rel="stylesheet" href="{{asset('login-form/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('login-form/css/style.css')}}">
    <title>Login &mdash; Farghani News</title>
</head>
<body>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 order-md-2">
                    <img src="{{asset('login-form/images/undraw_file_sync_ot38.svg')}}" alt="Image" class="img-fluid">
                </div>
            <div class="col-md-6 contents">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="mb-4">
                            <h3 style="color: #38d39f;">Sign In to <strong style="color: black;">Farghani News</strong></h3>
                            <p class="mb-4">Lorem ipsum dolor sit amet elit. Sapiente sit aut eos consectetur adipisicing.</p>
                        </div>
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                                <div class="form-group first">
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="d-flex mb-5 align-items-center">
                                    <label class="control control--checkbox mb-0" for="remember"><span class="caption">{{ __('Remember Me') }}</span>
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} /> {{--checked="checked"--}}
                                    <div class="control__indicator"></div>
                                    </label>
                                    <span class="ml-auto">
                                        @if (Route::has('password.request'))
                                            <a href="{{route('password.request')}}" class="forgot-pass"> {{ __('Forgot Your Password?') }}</a>
                                        @endif
                                    </span>
                                </div>
                                    <input type="submit" value="{{ __('Log In') }}" class="btn text-white btn-block btn-primary">
                                    <span class="d-block text-left my-2 text-muted">
                                        @if (Route::has('register'))
                                            <p>Belum punya akun? Daftar<a href="{{route('register')}}" class="forgot-pass"> {{ __('disini') }}</a></p>
                                        @endif
                                    </span>
                                    <span class="d-block text-left my-3 text-muted">{{ __('or sign in with') }}</span>
                                <div class="social-login">
                                    <a href="#" class="facebook">
                                    <span class="icon-facebook mr-3"></span>
                                </a>
                                    <a href="#" class="twitter">
                                    <span class="icon-twitter mr-3"></span>
                                </a>
                                    <a href="#" class="google">
                                    <span class="icon-google mr-3"></span>
                                </a>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <script src="{{asset('login-form/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('login-form/js/popper.min.js')}}"></script>
    <script src="{{asset('login-form/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('login-form/js/main.js')}}"></script>
</body>
</html>

{{-- test logging page  --}}
{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
