@extends('layouts.app')
@section('content')
@push('links')
<link rel="stylesheet" href="{{asset('user/css/custom.css')}}">

<style>
    .card-blog {
        margin-top: 30px;
    }

    .card-search {
        margin-bottom: 30px;
    }

    .card-post {
        margin-top: 30px;
    }
</style>
@endpush
<!-- Page Content -->
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
  <div class="container">

    <div class="row">
      <!-- Blog Entries Column -->
      <div class="col-md-8">

        <h1 class="my-4">Page Post
          {{-- <small>Secondary Text</small> --}}
        </h1>

        <!-- Blog Post -->
        @foreach ($posts as $post)
        <div class="card-blog mb-4">
            <img class="card-img-top" src="{{asset('storage/post/' .$post->gambar)}}" alt="{{$post->slug}}" width="750" height="300">
            <div class="card-body">
            <h2 class="card-title" style="margin-bottom:0;color:black;"><a href="{{route('user.read', $post->slug)}}">{{$post->title}}</a></h2> 
                <div class="card-footer text-muted" style="margin-bottom: 10px">
                    <i>Posted on {{ $post->created_at->format('F d, Y') }} by
                    <a href="#">{{$post->user->name }}</a></i>
                    <div class="list-category">
                      <div class="advance-btn">{{ $post->category->name }}</div>
                    </div>
                </div>
                <p class="card-text">{!! Str::limit($post->content, 10) !!}</p> {{--limit=400 --}}
            <a href="{{route('user.read', $post->slug)}}" class="btn btn-primary">Read More &rarr;</a>
            </div>
        </div>
        @endforeach
        <!-- Pager -->
        <div class="clearfix mt-4">
            {{$posts->links('vendor.pagination.bootstrap-4')}}
        </div>

      </div>

      <!-- Sidebar Widgets Column -->
      <div class="col-md-4">

        <!-- Search Widget -->
        <div class="card-search my-4">
          {{-- <h5 class="card-header">Search</h5> --}}
          <div class="card-body">
            <div class="input-group">
                <form class="form-search" method="get" action="#">
                    <input type="search" name="search" placeholder="search posts here..">
                    <button type="submit">Search</button>
                </form>
            </div>
          </div>
        </div>

        <!-- Category -->
        <div class="single category">
            <h3 class="side-title">Category</h3>
            <ul class="list-unstyled">
              {{-- @foreach ($categories as $category)
                <li><a href="" title="">{{$category->name}} <span class="d-flex justify-content-end">13</span></a></li>
              @endforeach --}}

              @forelse ($categories as $category)
              <li><a href="#">{{$category->name}}</a></li>
              @empty
              @endforelse
            </ul>
            <hr>
        </div>
          <!-- /Category -->

        <div class="card-post my-4">
            <div class="sidebar widget">
                <h3>Recent Post</h3>
                <ul>
                    @forelse ($latestPosts as $latest)
                    <li>
                        <div class="sidebar-thumb">
                        <img class="animated rollIn" src="{{asset('storage/post/' .$latest->gambar)}}" alt="{{$latest->title}}" />
                        </div><!-- .Sidebar-thumb -->
                        <div class="sidebar-content">
                            <h5 class="animated bounceInRight"><a href="{{route('user.read', $latest->slug)}}">{{$latest->title}}</a></h5>
                        </div><!-- .Sidebar-thumb -->
                            <div class="sidebar-meta">
                            <span class="time" ><i class="fa fa-clock-o"></i> {{ $latest->created_at->format('M d, Y') }}</span>
                            <span class="comment"><i class="fa fa-comment"></i> {{$latest->comments->count()}} comments</span>
                        </div><!-- .Sidebar-meta ends here -->
                    </li>
                    @empty
                        
                    @endforelse
            {{-- @foreach ($latest as $item)
                    <li>
                        <div class="sidebar-thumb">
                            <img class="animated rollIn" src="{{asset('storage/post/' .$item->gambar)}}" alt="" />
                        </div><!-- .Sidebar-thumb -->
                        <div class="sidebar-content">
                            <h5 class="animated bounceInRight"><a href="{{route('user.read', $post->slug)}}">{!! Str::limit($item->title, 25) !!}</a></h5>
                        </div><!-- .Sidebar-thumb -->
                        <div class="sidebar-meta">
                            <span class="time" ><i class="fa fa-clock-o"></i> {{ $post->created_at->format('M d, Y') }}</span>
                            <span class="comment"><i class="fa fa-comment"></i> {{$post->comments->count()}} comments</span>
                        </div><!-- .Sidebar-meta ends here -->
                    </li>
            @endforeach --}}
                  {{-- <li>
                      <div class="sidebar-thumb">
                          <img class="animated rollIn" src="http://shop.spotlayer.com/demo/soft-mag/demo1/wp-content/uploads/8S64npOgTu2eWTZIXEfy_DSC_0955-90x75.jpg" alt="" />
                      </div><!-- .Sidebar-thumb -->
                      <div class="sidebar-content">
                          <h5 class="animated bounceInRight"><a href="#">Watch Now: How to Manipulate the Media</a></h5>
                      </div><!-- .Sidebar-thumb -->
                          <div class="sidebar-meta">
                          <span class="time" ><i class="fa fa-clock-o"></i> Aug 27, 2015</span>
                          <span class="comment"><i class="fa fa-comment"></i> 10 comments</span>
                      </div><!-- .Sidebar-meta ends here -->
                  </li>
                  <li>
                      <div class="sidebar-thumb">
                          <img class="animated rollIn" src="http://shop.spotlayer.com/demo/soft-mag/demo1/wp-content/uploads/17153174816_a808becb66_o-90x75.jpg" alt="" />
                      </div><!-- .Sidebar-thumb -->
                      <div class="sidebar-content">
                          <h5 class="animated bounceInRight"><a href="#">OnePlus Inks First Carrier Partnership with Malaysia</a></h5>
                      </div><!-- .Sidebar-thumb -->
                      <div class="sidebar-meta">
                          <span class="time" ><i class="fa fa-clock-o"></i> Aug 27, 2015</span>
                          <span class="comment"><i class="fa fa-comment"></i> 10 comments</span>
                      </div><!-- .Sidebar-meta ends here -->
                  </li>
                  <li>
                      <div class="sidebar-thumb">
                          <img class="animated rollIn" src="http://shop.spotlayer.com/demo/soft-mag/demo1/wp-content/uploads/16986542958_6a9303388c_o-90x75.jpg" alt="" />
                      </div><!-- .Sidebar-thumb -->
                      <div class="sidebar-content">
                          <h5 class="animated bounceInRight"><a href="#">Heaps Wants to Enhance How Groups of Friends</a></h5>
                      </div><!-- .Sidebar-thumb -->
                      <div class="sidebar-meta">
                          <span class="time" ><i class="fa fa-clock-o"></i> Aug 27, 2015</span>
                          <span class="comment"><i class="fa fa-comment"></i> 10 comments</span>
                      </div><!-- .Sidebar-meta ends here -->
                  </li> --}}
                </ul>
            </div><!-- .Widget ends here -->
        </div><!-- .Col ends here -->


      </div>

    </div>
    <!-- /.row -->

  </div>
@endsection