<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // categories globally share to all view
        View::share('categories', Category::all());

        // Latest Post share to all view
        $latestPost = Post::latest()->take(4)->get();
        View::share('latestPosts', $latestPost);
    }
}
