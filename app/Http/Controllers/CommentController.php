<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\CommentReply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function store(Request $request, $post){
        $this->validate($request, ['comment' => 'required|max:1000']);
        $comment = new Comment();
        $comment->post_id = $post;
        $comment->user_id = Auth::id();
        $comment->comment = $request->comment;
        $comment->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);

        // delete with replies comment
        $replies = CommentReply::where('comment_id', $id)->delete();
        $comment->delete();

        return redirect()->back()->with('success', 'Comments was successfully delete!');
    }
}
