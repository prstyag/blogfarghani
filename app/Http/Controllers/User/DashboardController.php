<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Tag;
use App\Models\Post;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:owner|admin|blogwriter|user');
    }

    public function index()
    {
        $posts = Post::where('status', 'publish')
                        ->latest()->take(4)->get();
        return view('index', compact('posts'));
    } 
    
    public function posts()
    {
        // $categories = Category::all();
        $posts = Post::where('status', 'publish')
                        ->latest()->paginate(3);
        // $latest = Post::latest()->take(4)->get();
        return view('posts', compact('posts'));
    } 

    public function read($slug)
    {

        // $data = '';
        // $posts = Post::withCount('comments')->get();
        // $comment = Comment::withCount('replies')->get();
        // $total = $posts + $comment;
        
        // $data = [
        //     'total' => $total,
        //     // 'n_comment' => $total,
        //     // 'commentsCount' => $commentsCount,
        // ];
        
        // $n_comment = Post::withCount('comments')->get();
        // $n_reply = Comment::withCount('replies')->get();
        $posts = Post::where('slug', $slug)->first();
        $latest = Post::latest()->take(4)->get();
        $categories = Category::all();
        $tags = Tag::all();
        return view('single_post', compact('posts', 'tags', 'categories', 'latest'));
    } 
}
