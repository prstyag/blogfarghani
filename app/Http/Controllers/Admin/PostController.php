<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:owner|admin|blogwriter');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::with('posts')->get();
        $posts = Post:: where('status', 'publish')
                        ->orderBy('created_at', 'desc')->paginate(8);
        return view('admin.post.index', compact('posts','tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        $posts = Post::paginate(8);
        return view('admin.post.create', compact('posts', 'tags', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'title' => 'required',
            'category_id' => 'required',
            'status' => 'required',
            'content' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1048',
        ]);
        

        if ($request->hasFile('gambar')) {
            // request file
            $gambar = $request->file('gambar');
            // rename file with math
            $math = rand(10000, 99999);
            // named file
            $image_name = time(). + $math.'.'.$gambar->extension();
            // request path disk file 
            $request->gambar->storeAs('post', $image_name, 'public');
        } else {
            $image_name = 'noThumbnail.png';
        }

        // Checking value status 

        // if ($request->status  != 'draft') {
        //     dd('Hooolaaaaa Publish!');
        // } else {
        //     dd('Hooolaaaaa Draft!');
        // }
        
        
        // $gambar = $request->file('gambar');
        // $math = rand(10000, 99999);
        // $image_name = time(). + $math.'.'.$gambar->extension();
        // $request->gambar->storeAs('post', $image_name, 'public');

        $posts = new Post();
        $posts->title = $request->input('title');
        $posts->slug = str_slug( $request->input('title'));
        $posts->category_id = $request->input('category_id');
        $posts->content = $request->input('content');
        $posts->status = $request->input('status');
        $posts->gambar = $image_name;
        $posts->user_id = Auth::user()->id;
        // dd($posts);
        $posts->save();

        $posts->tags()->attach($request->tags);
        return redirect()->back()->with('success', 'New Post was successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $categories = Category::all();
        $tags = Tag::all();
        $posts = Post::where('slug', $slug)->first();
        return view('admin.post.show', compact('posts', 'tags', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $categories = Category::all();
        $tags = Tag::all();
        $posts = Post::where('slug', $slug)->first();
        return view('admin.post.edit', compact('posts', 'tags', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'title' => 'required',
            'category_id' => 'required',
            'content' => 'required',
        ]);

        $posts = Post::where('slug', $slug)->first();
        if($request->hasFile('gambar')){
            $gambar = $request->file('gambar');
            $image_name = time().'.'.$gambar->getClientOriginalName();

            // Delete old images
            //

            $request->gambar->storeAs('post', $image_name, 'public');

            $posts->title = $request->input('title');
            $posts->category_id = $request->input('category_id');
            $posts->content = $request->input('content');
            $posts->slug = str_slug($request->input('title'));
            $posts->gambar =$image_name;
        } 
        else {
            $posts->title = $request->input('title');
            $posts->category_id = $request->input('category_id');
            $posts->content = $request->input('content');
            $posts->slug = str_slug($request->input('title'));
        }

        $posts->tags()->sync($request->tags);
        $posts->update();
        
        return redirect('/admin/post')->with('success', 'Post was successfully update!');
    }


    public function draftPost(Request $request){
        // $data = [];
        // $n_draft = Post::where('status', 'draft')
        //                 ->orderBy('created_at', 'desc')
        //                 ->paginate(8);
        // $data = [
        //     'draft' => $n_draft,

        // ];
        $tags = Tag::with('posts')->get();
        $posts = Post:: where('status', 'draft')
                        ->orderBy('created_at', 'desc')
                        ->paginate(8);

        return view('admin.post.draft', compact('tags', 'posts'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $posts = Post::where('slug', $slug)->first();
        $posts->delete();

        return redirect()->back()->with('success', 'Post was successfully delete!');
    }

    public function trashed(){
        $posts = Post::onlyTrashed()->paginate(8);
        return view('admin.post.trashed', compact('posts'));
    }

    public function restore($id){
        $posts = Post::withTrashed()->where('id', $id)->first();
        $posts->restore();

        return redirect()->back()->with('success', 'Post was successfully restored!');
    }

    public function kill($id){
        $posts = Post::withTrashed()->where('id', $id)->first();

        //Delete old images from directory file 
        if(file_exists(public_path('storage/post/'))){
            unlink(public_path('storage/post/' .$posts->gambar));
        };

        $posts->forceDelete();

        return redirect()->back()->with('success', 'Post was successfully Delete!');
    }


    // private function statusPost(){
    //     return[
    //         'draft' => trans('posts.form_control.select.status.option.draft'),
    //         'publish' => trans('posts.form_control.select.status.option.publish'),
    //     ];
    // }
}
