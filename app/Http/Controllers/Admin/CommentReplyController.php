<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CommentReply;
use Illuminate\Http\Request;

class CommentReplyController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:owner|admin|blogwriter');
    }
    
    public function index()
    {
        $replies = CommentReply::orderBy('created_at', 'desc')->paginate(8);
        return view('admin.post_comment_reply.index', compact('replies'));
    }

    public function destroy($id)
    {
        $replies = CommentReply::findOrFail($id);
        $replies->delete();

        return redirect()->back()->with('success', 'Reply-comment was successfully delete!');
    }
}
