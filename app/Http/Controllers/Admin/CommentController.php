<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\CommentReply;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:owner|admin|blogwriter');
    }
    
    public function index()
    {
        $comments = Comment::orderBy('created_at', 'desc')->paginate(8);
        return view('admin.post_comment.index', compact('comments'));
    }

    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);

        // delete with replies comment
        $replies = CommentReply::where('comment_id', $id)->delete();
        $comment->delete();

        return redirect()->back()->with('success', 'Comments was successfully delete!');
    }
}
