<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function store(Request $request){
        if(!Auth::guard('admin')->attempt($request->only('email','password'), $request->filled('remember'))){
            throw ValidationException::withMessages([
                'email' => 'Invalid email or password'
            ]);
        }
        // return redirect()->back();
        return redirect()->intended((route('admin.welcome')));
    }
}
