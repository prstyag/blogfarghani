<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Post;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:owner|admin|blogwriter');
    }

    public function index()
    {
        $data = [];
        $n_users = User::all()->count();

        // Count all Roles Blogwritter
        $n_blogwr = User::with('roles')->whereHas('roles', function($query) {
            $query->where('name', 'Blogwriter');
            })->count();

        // Count all Roles Administrator
        $n_admin = User::with('roles')->whereHas('roles', function($query) {
            $query->where('name', 'admin');
            })->count();
        $n_post = Post::all()->count();
        $data = [
            'n_users' => $n_users,
            'n_post' => $n_post,
            'n_blogwr' => $n_blogwr,
            'n_admin' => $n_admin,

        ];
        return view('admin.dashboard', $data);
    }

    public function showProfile()
    {
        $user = User::find(Auth::user()->id);
        return view('admin.users.profile.index', compact('user'));
    }

    public function editProfile()
    {
        $user = User::find(Auth::user()->id);
        return view('admin.users.profile.edit', compact('user'));
    }

    public function updateProfile(Request $request, $id)
    {
            $this->validate($request, [
                'name' => 'required',
                'username' => ['required', 'string', 'min:2', 'max:255'],
                'address' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'regex:/^((62)|(08))[0-9]/', 'not_regex:/[a-z]/', 'min:11', 'max:13'],
                'email' => 'required|email|unique:users,email,' . $id,
                'photo' => 'sometimes|image|mimes:.jpg,png,jpeg|max:9000',
            ]);

            $user = User::findOrFail(Auth::user()->id);
            if($request->hasFile('photo')){
                $photo = $request->file('photo');
                $filename = time().'.'.$photo->getClientOriginalName();
                
                // Delete old images
                if(auth()->user()->photo){
                    Storage::delete('/public/photo/' . auth()->user()->photo);
                }

                $request->photo->storeAs('photo', $filename, 'public');



                $user->name = $request->input('name');
                $user->username = $request->input('username');
                $user->address = $request->input('address');
                $user->phone = $request->input('phone');
                $user->email = $request->input('email');
                $user->photo =$photo;

                $user->update(['photo' => $filename]);
            }

            return redirect()->route('admin.user.profile')->with('success', "Profile $user->name has successfully been updated.");
    }

}
