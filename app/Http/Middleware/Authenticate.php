<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Route;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        $name = $request->path();
        $name = explode("/", $name);
        // dd($name);

        if (! $request->expectsJson()) {
            if($name[0] == "admin"){
                return route('admin.');
            }else{
                return route('login');
            }
            // return route('login');
        }
    }
}
