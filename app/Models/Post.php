<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Null_;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['title', 'category_id', 'user_id', 'content', 'gambar', 'slug'];
    public $timestamps = true;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getUrlAttribute(){
        $hasMedia =  $this->getMedia('gambar')->first();
        return $hasMedia != null ?
            $hasMedia->getUrl() : "";
    }

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function tags(){
        return $this->belongsToMany('App\Models\Tag');
    }

    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }

    // public function getTotalComments(){
    //     return $this->comments->count();
    // }
    
    // public function replies()
    // {
    //     return $this->hasMany('App\Models\CommentReply');
    // }
}
