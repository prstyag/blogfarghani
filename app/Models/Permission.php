<?php

namespace App\Models;

use Laratrust\Models\LaratrustPermission;

class Permission extends LaratrustPermission
{
    public $guarded = [];

    public function user() {
        return $this->belongsToMany(Role::class);
    }

    public function roles() {
        return $this->belongsToMany(Permission::class);
    }
}
