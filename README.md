### Installation

```sh
git clone https://gitlab.com/prstyag/blogfarghani.git
```

Install Composer Dependencies

```sh
composer install
```

Create a copy of your .env file

```sh
cp .env.example .env
```

Generate an app encryption key

```sh
php artisan key:generate
```

Create an empty database for our application and set yo environment variable in .env file

```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=your_database
DB_USERNAME=root
DB_PASSWORD=
```

Then migrate the database.
Go on the console and type..

```sh
php artisan migrate --seed | php artisan db:seed
```

This allows you to have necessary login credential

> Owner Credential
> email : owner@farghani.com
> password: password

> Admin Credential
> email : admin@farghani.com
> password: password

> User Credential
> email : user@farghani.com
> password: password

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Supporting

Please see [Farghani](https://farghani.com/) for details.

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
